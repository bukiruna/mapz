﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab3
{
    public class ModelTests
    {
        public List<BranchOfDevelopment> Branches;
        public List<Building> Buildings;

        public ModelTests()
        {
            Branches = new List<BranchOfDevelopment>()
            {
                new BranchOfDevelopment(type: "Social", tasksToComplete: 20, buildingsToComplete: 9),
                new BranchOfDevelopment(type: "Economic", tasksToComplete: 18,buildingsToComplete: 7),
                new BranchOfDevelopment(type : "Cultural and Entertainment", tasksToComplete : 15, buildingsToComplete : 6),
                new BranchOfDevelopment(type : "Environmental", tasksToComplete : 14, buildingsToComplete : 4),
                new BranchOfDevelopment(type : "Innovative Technologies", tasksToComplete : 22, buildingsToComplete : 5),
                new BranchOfDevelopment(type : "Urban Planning", tasksToComplete : 25, buildingsToComplete : 13)
            };

            Buildings = new List<Building>()
            {
                new Building(type: "Office", branch: Branches[1]),
                new Building(type: "Shop", branch: Branches[1]),
                new Building(type: "Restaurant", branch: Branches[1]),
                new Building(type: "College", branch: Branches[0]),
                new Building(type: "Hospital", branch: Branches[0]),
                new Building(type: "Religious", branch: Branches[0]),
                new Building(type: "Gym", branch: Branches[2]),
                new Building(type: "Theater", branch: Branches[2]),
                new Building(type: "Cinema", branch: Branches[2]),
                new Building(type: "Park", branch: Branches[3]),
                new Building(type: "Houses", branch: Branches[5]),
                new Building(type: "Laboratories", branch: Branches[4])
            };
        }

        public void ShowData()
        {
            var strBuilder = new StringBuilder();

            Buildings.ForEach(building =>
                strBuilder.Append(String.Format(
                    "Type of building: " + building.Type + ", Branch ID: " + building.BranchId + 
                    ", Branch name: " + (Branches.Find(branch => branch.BranchId == building.BranchId)?.Type ?? "Unknown") +
                    ", Buildings to complete branch:" + (Branches.Find(branch => branch.BranchId == building.BranchId)?.BuildingsToComplete ?? 0) +"\n"
                   )));
            Console.WriteLine(strBuilder.ToString());
        }
    }

    public class BuildingsComparator : IComparer<Building>
    {
        public int Compare(Building building1, Building building2)
        {
            int typeComparison = string.Compare(building1?.Type, building2?.Type, StringComparison.Ordinal);
            return typeComparison != 0 ? typeComparison : (building1?.BuildingId.CompareTo(building2?.BuildingId) ?? 0);
        }
    }
}
