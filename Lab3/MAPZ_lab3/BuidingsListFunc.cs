﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab3
{
    public static class BuidingsListFunc
    {
        public static SortedList<TGuid, TBuilding> ToSortedList<TGuid, TBuilding>
            (this List<TBuilding> levelsList, Func<TBuilding, TGuid> selector)
        {
            var sortedBuidingList = new SortedList<TGuid, TBuilding>();
            levelsList.ForEach(level => sortedBuidingList.Add(selector(level), level));
            return sortedBuidingList;
        }

        public static Queue<TBuilding> ToQueue<TBuilding>(this List<TBuilding> buildList)
        {
            return new Queue<TBuilding>(buildList);
        }

        public static Stack<TBuilding> ToStack<TBuilding>(this List<TBuilding> buildList)
        {
            var buildingsStack = new Stack<TBuilding>();
            buildList.ForEach(level => buildingsStack.Push(level));
            return buildingsStack;
        }
    }
}
