﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab3
{
    public class Building
    {
        public string Type { get; }
        public Guid BuildingId { get; }
        public Guid BranchId { get; }

        public Building(string type, BranchOfDevelopment branch)
        {
            Type = type;
            BuildingId = Guid.NewGuid();
            BranchId = branch.BranchId;
        }

    }
}
