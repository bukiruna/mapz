﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab3
{
    public static class BranchFunc
    {
        public static int GetTotalAmountOfTasks(this BranchOfDevelopment branch)
        {
            int res = branch.TasksToComplete + branch.BuildingsToComplete;
            return res;
        }
    }
}
