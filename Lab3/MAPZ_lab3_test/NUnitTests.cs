﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using MAPZ_lab3;


namespace MAPZ_lab3_test
{
    [TestFixture]
    public class NUnitTests
    {
        private ModelTests model;

        [SetUp]
        public void Setup()
        {
            model = new ModelTests();
        }

        // --------------------

        [Test]
        public void TestSelect()
        {
            var arr = model.Buildings.Select(bld => bld.Type).ToArray();

            Assert.That(arr[arr.Length - 1], Is.EqualTo(model.Buildings[model.Buildings.Count - 1].Type));
        }

        [Test]
        public void TestWhere()
        {
            var arr = model.Branches.Where(branch => branch.BuildingsToComplete == 15).ToArray();
            var branchesWith15Bldng = model.Branches.Count(branch => branch.BuildingsToComplete == 15);

            Assert.That(arr.Length, Is.EqualTo(branchesWith15Bldng));
        }

        [Test]
        public void TestListOperations()
        {
            var bldng = model.Buildings;
            bldng.Sort((bld1, bld2) => bld1.Type.CompareTo(bld2.Type));

            Assert.That(bldng[0].Type, Is.EqualTo("Cinema"));
        }

        [Test]
        public void TestDictionaryOperations()
        {
            var branchesDict = model.Branches.ToDictionary( branch => branch.BranchId, branch => branch);

            var existingKey = model.Branches[0].BranchId;
            Assert.That(branchesDict.ContainsKey(existingKey), Is.True);
            
        }

        [Test]
        public void TestExtensionMethod()
        {
            var branch = new BranchOfDevelopment("Economic", 23, 10);
           
            Assert.That(branch.GetTotalAmountOfTasks(), Is.EqualTo(33));
        }

        [Test]
        public void TestAnonymous()
        {
            var anonimous = model.Branches.Select( branch => new {branch.Type, branch.TasksToComplete, branch.BuildingsToComplete});
            

            Assert.That(anonimous.Count, Is.EqualTo(model.Branches.Count));
        }

        [Test]
        public void TestCompaison()
        {
            var comparator = new BuildingsComparator();

            var sortedBuildings = model.Buildings.OrderBy(build => build, comparator).ToList();

            for (int i = 0; i < sortedBuildings.Count - 1; i++)
            {
                Assert.That(comparator.Compare(sortedBuildings[i], sortedBuildings[i + 1]), Is.LessThanOrEqualTo(0));
            }
            
        }

        [Test]
        public void TestToArray()
        {
            var buildings = model.Buildings.Where(building => building.Type != "Hospital").ToArray();
            Assert.That(buildings.Length, Is.EqualTo(model.Buildings.Count - 1));
        }

        [Test]
        public void TestSorting()
        {
            var buildingsSorted = model.Buildings.OrderBy(building => building.Type).Select(building => building.Type);

            Assert.That(buildingsSorted, Is.Not.Null);
            Assert.That(buildingsSorted, Is.Ordered);
        }


        // ---------------------

        [Test]
        public void TestBranchesDictionary()
        {
            var BranchesDict = model.Branches.ToDictionary(
                branch => branch.BranchId,
                branch => branch
            );

            Assert.That(BranchesDict.Keys, Is.All.TypeOf<Guid>());
        }

        [Test]
        public void TestBuildingsDictionary()
        {
            var buildinsDict = model.Buildings.ToDictionary(
                bld => bld.BuildingId,
                bld => bld
            );

            Assert.That(buildinsDict.Keys, Is.All.TypeOf<Guid>());
        }

        [Test]
        public void TestBuildingsSortedList()
        {
            var buildSortedList = model.Buildings.ToSortedList(building => building.BuildingId);

            Assert.That(buildSortedList, Is.Not.Null);
            Assert.That(buildSortedList.Count, Is.EqualTo(model.Buildings.Count));
        }

        [Test]
        public void TestBuildingsQueue()
        {
            var buidingsQueue = model.Buildings.ToQueue();

            Assert.That(buidingsQueue, Is.Not.Null);
            var queueTypes = buidingsQueue.Select(level => level.Type);
            Assert.That(queueTypes, Is.EquivalentTo(model.Buildings.Select(building => building.Type)));
        }

        [Test]
        public void TestBuidingsStack()
        {
            var buildingStack = model.Buildings.ToStack();

            Assert.That(buildingStack, Is.Not.Null);

            var stack = new List<string>();
            while (buildingStack.Count > 0)
            {
                stack.Add(buildingStack.Pop().Type);
            }

            Assert.That(stack, Is.EquivalentTo(model.Buildings.Select(building => building.Type)));
        }

        [Test]
        public void TestGroupBy()
        {
            var groupedById = model.Buildings.GroupBy(buiding => buiding.BranchId);
            Assert.That(groupedById.Count(), Is.EqualTo(model.Branches.Count()));
        }

        [Test]
        public void TestComplicatedOperations()
        {
            var branchesById = model.Branches.ToDictionary(b => b.BranchId, b => b);
            var sortBldng = model.Buildings.OrderBy(bld => branchesById[bld.BranchId].BuildingsToComplete).Select(bld => bld.Type).ToArray();
            

            Assert.That(sortBldng, Is.Not.Null);
            Assert.That(sortBldng[0], Is.EqualTo("Park"));
        }
    }
}