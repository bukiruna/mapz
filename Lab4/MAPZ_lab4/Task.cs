﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab4
{
    public class Task
    {
        public string TaskText { get; set; }

        public int Count { get; set; }

        public string TaskType { get; set; }

        public Task(string text, string type)
        {
            this.TaskText = text;
            TaskType = type;
        }

        public Task(string text, int count, string type)
        {
            this.TaskText = text;
            this.Count = count;
            this.TaskType = type;
        }

        public Task(string text, int count)
        {
            TaskText = text;
            Count = count;
        }

        public Task()
        {

        }

    }
}
