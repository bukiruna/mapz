﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab4
{
    public interface ITaskFactory
    {
        ICreationalTask CreateCreationalTask();

        ISupportTask CreateSupportTask();
    }
    public interface ICreationalTask
    {
        Task CreateCreationalTask();
    }

    public interface ISupportTask
    {
        Task CreateSupportTask();
    }

    class ConcreteUrbanFactory : ITaskFactory
    {
        public ICreationalTask CreateCreationalTask()
        {
            return new ConcreteCreationalTaskUrban();
        }

        public ISupportTask CreateSupportTask()
        {
            return new ConcreteSupportTaskUrban();
        }
    }

    class ConcreteStrategyFactory : ITaskFactory
    {
        public ICreationalTask CreateCreationalTask()
        {
            return new ConcreteCreationalTaskMillitary();
        }

        public ISupportTask CreateSupportTask()
        {
            return new ConcreteSupportTaskEnergy();
        }
    }

    class ConcreteCreationalTaskMillitary : ICreationalTask
    {
        public Task CreateCreationalTask()
        {
            return new Task("Create 2 millitary buildings", 2, "Millitary");
        }
    }

    class ConcreteCreationalTaskUrban : ICreationalTask
    {
        public Task CreateCreationalTask()
        {
            return new Task("Create 4 urban buildings", 4, "Urban");
        }
    }

    class ConcreteSupportTaskUrban : ISupportTask
    {
        public Task CreateSupportTask()
        {
            return new Task("Get 3 more urban buildings", 3, "Urban");
        }
    }

    class ConcreteSupportTaskEnergy : ISupportTask
    {
        public Task CreateSupportTask()
        {
            return new Task("Get 5 more Energy", 5, "Energy");
        }

    }

}
