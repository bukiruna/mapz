﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Threading.Tasks;

namespace MAPZ_lab4
{
    public class Building : IBuildingPrototype
    {
        public BuildingType Type { get; set; }

        
        
        public Guid BuildingId { get; set; }
        public Image icon { get; set; }

        public Building() { }
        public Building(BuildingType type, Image icon)
        {
            Type = type;
            BuildingId = Guid.NewGuid();
            this.icon = icon;
        }
       
        public IBuildingPrototype Clone()
        {
            return new Building() { Type = Type, icon = icon,  BuildingId = Guid.NewGuid() };
        }
    }

    public enum BuildingType
    {
        Military,
        Energy,
        Urban,
        Hospital,
        School
    }
   
}
