﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab4
{
    public class BranchOfDevelopment
    {
        public string Type { get; }
        public Guid BranchId { get; }

        public int TasksToComplete { get; }

        public int BuildingsToComplete { get; }

        public BranchOfDevelopment(string type, int tasksToComplete, int buildingsToComplete)
        {
            Type = type;
            BranchId = Guid.NewGuid();
            TasksToComplete = tasksToComplete;
            BuildingsToComplete = buildingsToComplete;
        }


    }
}
