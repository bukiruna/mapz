﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using static System.Net.Mime.MediaTypeNames;

namespace MAPZ_lab4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        
        public MainWindow()
        {
            InitializeComponent();
            InitializeCity();
            InitializeBuildings();
            Canvas.MouseRightButtonDown += Canvas_MouseRightButtonDown;
        }

       
        private MilitaryBuilding militaryBuilding;
        private EnergyBuilding energyBuilding;
        private UrbanBuilding urbanBuilding;
        private Hospital hospital;
        private School school;

        private void InitializeBuildings()
        {
            Image military = new Image();
            military.Source = new BitmapImage(new Uri("military.png", UriKind.RelativeOrAbsolute));
            militaryBuilding = new MilitaryBuilding(BuildingType.Military, military);

            Image energy = new Image();
            energy.Source = new BitmapImage(new Uri("energetic.png", UriKind.RelativeOrAbsolute));
            energyBuilding = new EnergyBuilding(BuildingType.Energy, energy);

            Image urban = new Image();
            urban.Source = new BitmapImage(new Uri("house.png", UriKind.RelativeOrAbsolute));
            urbanBuilding = new UrbanBuilding(BuildingType.Urban, urban);

            Image hosp = new Image();
            hosp.Source = new BitmapImage(new Uri("hospital.png", UriKind.RelativeOrAbsolute));
            hospital = new Hospital(BuildingType.Hospital, hosp);

            Image schl = new Image();
            schl.Source = new BitmapImage(new Uri("school.png", UriKind.RelativeOrAbsolute));
            school = new School(BuildingType.School, schl);
        }

        private CityManager city;
        private void InitializeCity()
        {
            city = CityManager.Instance;
            CityName_Lbl.Content = city.Name;
            Area_Lbl.Content += Convert.ToString(city.Area) + "km^2";
            Population_Lbl.Content += Convert.ToString(city.Population); 
        }

        private void Canvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point point = e.GetPosition(Canvas);
            BuildWindow secondWindow = new BuildWindow();
            secondWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            secondWindow.ShowDialog();
            string receivedValue = secondWindow.ValueToSend;

            PlaceBuilding(receivedValue, point);
        }

        private void PlaceBuilding(string type, Point point)
        {
            Image image = new Image();
            if (type == "Military")
            {
                MilitaryBuilding building = (MilitaryBuilding)militaryBuilding.Clone();
                image = building.icon;
            }
            if (type == "Energy")
            {
                EnergyBuilding building = (EnergyBuilding)energyBuilding.Clone();
                image = building.icon;
            }
            if (type == "Urban")
            {
                UrbanBuilding building = (UrbanBuilding)urbanBuilding.Clone();
                image = building.icon;
                Random random = new Random();
                int rand = random.Next(1, 5);
                city.Population += rand;
                Population_Lbl.Content = "Population: " + city.Population;
            }
            if (type == "Hospital")
            {
                Hospital building = (Hospital)hospital.Clone();
                image = building.icon;
            }
            if (type == "School")
            {
                School building = (School)school.Clone();
                image = building.icon;
            }

            image.Width = 50;
            image.Height = 50;

            Canvas.SetLeft(image, point.X - image.Width/2);
            Canvas.SetTop(image, point.Y - image.Height);
            //ексепшн якшо пару разів то саме додавати
            Canvas.Children.Add(image);
        }
        bool IsTasksHidden = true;
        private void Tasks_Btn_Click(object sender, RoutedEventArgs e)
        {

            if (IsTasksHidden)
            {
                
                Tasks_List.Visibility = Visibility.Visible;
                for (int i = 0; i < 2; i++)
                {
                    
                    Task task = GenerateTask();
                    Tasks_List.Text += task.TaskText + "\n";

                    
                }

                
            }
            if(!IsTasksHidden)
            {
                Tasks_List.Visibility = Visibility.Hidden;
                Tasks_List.Clear();
            }
            IsTasksHidden = !IsTasksHidden;
        }

        private Task GenerateTask()
        {
            Random random = new Random();

            int typeTask = random.Next(0, 2);
            int prodTask = random.Next(0, 2);

            ITaskFactory taskFactory;

            if (typeTask == 0)
            {
                taskFactory = new ConcreteUrbanFactory();
            }
            else taskFactory = new ConcreteStrategyFactory();

            Task task;

            if (prodTask == 0)
            {
                task = taskFactory.CreateCreationalTask().CreateCreationalTask();
            }
            else task = taskFactory.CreateSupportTask().CreateSupportTask();

            return task;
        }
    }
}
