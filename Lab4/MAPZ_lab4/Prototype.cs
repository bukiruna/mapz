﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.TextFormatting;
//using static System.Net.Mime.MediaTypeNames;
using System.Windows.Media.Imaging;

namespace MAPZ_lab4
{
    public interface IBuildingPrototype
    {
        BuildingType Type { get; set; }
        Image icon { get; set; }
        Guid BuildingId { get; }

        IBuildingPrototype Clone();
    }

    public class MilitaryBuilding : Building
    {
        public MilitaryBuilding() { }
        public MilitaryBuilding(BuildingType type, Image icon) : base(type, icon) {
            
        }

        public IBuildingPrototype Clone()
        {
            return new MilitaryBuilding() { Type = Type, icon = icon, BuildingId = Guid.NewGuid() };
        }
    }
    public class EnergyBuilding : Building
    {
        public EnergyBuilding() { }
        public EnergyBuilding(BuildingType type, Image icon) : base(type, icon) {
            
        }

        public IBuildingPrototype Clone()
        {
            return new EnergyBuilding() { Type = Type, icon = icon, BuildingId = Guid.NewGuid() };
        }
    }
    public class UrbanBuilding : Building
    {
        public UrbanBuilding() { }
        public UrbanBuilding(BuildingType type, Image icon) : base(type, icon) {
           
        }

        public IBuildingPrototype Clone()
        {
            return new UrbanBuilding() { Type = Type, icon = icon, BuildingId = Guid.NewGuid() };
        }
    }
    public class Hospital : Building
    {
        public Hospital() { }
        public Hospital(BuildingType type, Image icon) : base(type, icon) {
        }

        public IBuildingPrototype Clone()
        {
            return new Hospital() { Type = Type, icon = icon , BuildingId = Guid.NewGuid() };
        }
    }
    public class School : Building
    {
        public School() { }
        public School(BuildingType type, Image icon) : base(type, icon) {
        }

        public IBuildingPrototype Clone()
        {
            return new School() { Type = Type, icon = icon , BuildingId = Guid.NewGuid() };
        }
    }


}
