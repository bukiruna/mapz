﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab4
{
    public class CityManager
    {
        private static CityManager instance;
        public string Name { get; set; }
        public double Area { get; set; }
        public int Population { get; set; }
        public CityManager()
        {
            Name = "NovaCity";
            Area = 10;
            Population = 0;
        }
        public static CityManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CityManager();
                }
                return instance;
            }
        }

        public int IncreasePopulation(int amount)
        {
            Population += amount;
            return Population;
        }
        public int DecreasePopulation(int amount)
        {
            Population -= amount;
            return Population;
        }
        public double IncreaseArea(double amount)
        {
            Area += amount;
            return Area;
        }
        public double DecreaseArea(double amount)
        {
            Area -= amount;
            return Area;
        }

    }
}
