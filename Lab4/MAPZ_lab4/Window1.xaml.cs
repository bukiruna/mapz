﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MAPZ_lab4
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class BuildWindow : Window
    {
        public string ValueToSend { get; set; }
        public BuildWindow()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Build_btn_Click(object sender, RoutedEventArgs e)
        {
            if (Military.IsChecked == true) { ValueToSend = "Military"; }
            if (Energy.IsChecked == true) { ValueToSend = "Energy"; }
            if (Urban.IsChecked == true) { ValueToSend = "Urban"; }
            if (Hospital.IsChecked == true) { ValueToSend = "Hospital"; }
            if (School.IsChecked == true) { ValueToSend = "School"; }
            
            this.Close();
        }
    }
}
