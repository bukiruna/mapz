﻿using System;

interface IAnimal
{
    void ShowAnimal();
}

abstract class Animal
{
    protected string Name;
    protected int Age;
    

    protected Animal(string name, int age)
    {
        Name = name;
        Age = age;
    }

    public abstract void Feed();
}

internal class Cat: Animal, IAnimal
{
    private bool IsFluffyWool;
    public bool IsFed;

    public Cat(string name, int age, bool isFluffyWool, bool isFed): base(name, age)
    {
        IsFluffyWool = isFluffyWool;
        IsFed = isFed;
    }

    void IAnimal.ShowAnimal()
    {
        Console.WriteLine("Cat`s name is " + Name + ", age is " + Age + "y.o.");
    }
    
    public override void Feed()
    {
        Console.WriteLine("Cat " + Name + " is fed.");
    }

    public void Sleep()
    {
        Console.WriteLine("Cat " + Name + "is sleeping.");
    }
}