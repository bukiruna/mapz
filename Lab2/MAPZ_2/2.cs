﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_2
{
    internal class _2
    {
        static void Main()
        {
            Cat cat = new Cat("Murka", 3, true, false);

            cat.IsFed = true;

            cat.IsFluffyWool = false;

            cat.Age = 5;
        }
    }
}
