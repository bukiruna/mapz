﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_2
{
    internal class _10
    {
        static void BoxingUnBoxing()
        {
            int i = 10;
            object b = i; // Boxing
            Console.WriteLine("Boxing:"+b);

            b = 20;  
            int c = (int)b; // Unboxing
            Console.WriteLine("Unboxing:" + c);
        }
    }
}
