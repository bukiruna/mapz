﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_2
{
    internal class _11
    {
        static void Main(string[] args)
        {
            Temperature temp1 = new Temperature(25); 
            double celsiusValue = (double)temp1; // Явне

            Temperature temp2 = 32.0; // Неявне
        }
    }
    class Temperature
    {
        public double Celsius { get; set; }

        public Temperature(double celsius)
        {
            Celsius = celsius;
        }

        // Явне приведення 
        public static explicit operator double(Temperature temp)
        {
            return temp.Celsius;
        }

        // Неявне приведення
        public static implicit operator Temperature(double celsius)
        {
            return new Temperature(celsius);
        }
    }
}
