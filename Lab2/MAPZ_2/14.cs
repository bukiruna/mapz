﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_2
{
    using System;

    public class MyClass : Object
    {
        private int value;
        
        public MyClass(int value)
        {
            this.value = value;
        }

        // Перевизначення методу ToString()
        public override string ToString()
        {
            return $"Value: {value}";
        }

        // Перевизначення методу Equals()
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            MyClass other = (MyClass)obj;
            return value == other.value;
        }

        // Перевизначення методу GetHashCode()
        public override int GetHashCode()
        {
            return value.GetHashCode();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyClass obj1 = new MyClass(10);
            MyClass obj2 = new MyClass(10);

            bool a = obj1.Equals(obj2);
            int b = obj1.GetHashCode();
            string c = obj1.ToString();

        }
    }

}
