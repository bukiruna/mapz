﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_2
{

    enum Color
    {
        White = 1,
        Black = 2,
        Red = 3,
        Yellow = 4,
        Orange = Red | Yellow,
        NotBlack = ~Black,
        GreenAndWhite = 6,
        Green = GreenAndWhite ^ White,
        Invisible = 0
    }

    internal class _5
    {
        static void Main()
        {
            Color c = Color.White & Color.Black;

            if (c == 0 && Color.Green != 0) 
            {
                Console.WriteLine("Color is invisible and green");
            }
            else if(c == 0 || Color.Red != 0)
            {
                Console.WriteLine("Color is invisible or red");
            }
        }
    }
}
