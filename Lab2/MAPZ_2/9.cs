﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_2
{
    internal class _9
    {
        static void Change(int num)
        {
            num = 10;
        }
        static void ChangeRef(ref int num)
        {
            num = 10;
        }
        static void ChangeOut(out int num)
        {
            num = 10;
        }
        static void RefAndOut()
        {
            int a = 1;
            Change(a);
            Console.WriteLine("number without ref and out:" + a);

            int b = 1;
            ChangeRef(ref b);
            Console.WriteLine("number with ref:" + b);

            int c;
            ChangeOut(out c);
            Console.WriteLine("number with out:" + c);
        }
    }
}
