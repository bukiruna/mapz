using System;
using System.Diagnostics;
using System.Net.NetworkInformation;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;


public interface IA
{
    public void IDoSmth();
}

class BaseClass
{
    public BaseClass() { }
    public virtual void DoSmth() { }
}
class InheritedClass1 : BaseClass, IA
{
    public InheritedClass1() { }
    void IA.IDoSmth() { }
    public override void DoSmth() { }
}
class InheritedClass2 : InheritedClass1
{
    public InheritedClass2() { }

    public override void DoSmth() { }
}
class InheritedClass3 : InheritedClass2
{
    public InheritedClass3() { }

    public override void DoSmth() { }
}





public class PolymorphismBenchmark
{


    private IA interfaceObj;
    private BaseClass classObj;
    

    private static int N = 10000000;
    [GlobalSetup]
    public void Setup()
    {
        interfaceObj = new InheritedClass1();
        classObj = new InheritedClass3();
        
    }

    [Benchmark]
    public void VirtualMetod()
    {
        for (int i = 0; i < N; i++)
        {
            classObj.DoSmth();
        }
    }



    [Benchmark]
    public void IntefaceMetod()
    {
        for (int i = 0; i < N; i++)
        {
            interfaceObj.IDoSmth();
        }
    }

    [Benchmark]
    public void ReflectionMethod()
    {

        for (int i = 0; i < N; i++)
        {
            var methodInfo = typeof(InheritedClass3).GetMethod("DoSmth");
            methodInfo.Invoke(new InheritedClass3(), null);
        }
        

        
    }


}

class Program
{
    static void Main(string[] args)
    {
        var summary = BenchmarkRunner.Run<PolymorphismBenchmark>();
    }
}
