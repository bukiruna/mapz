```

BenchmarkDotNet v0.13.12, Windows 11 (10.0.22631.3296/23H2/2023Update/SunValley3)
11th Gen Intel Core i5-1135G7 2.40GHz, 1 CPU, 8 logical and 4 physical cores
.NET SDK 7.0.302
  [Host]     : .NET 6.0.16 (6.0.1623.17311), X64 RyuJIT AVX2
  DefaultJob : .NET 6.0.16 (6.0.1623.17311), X64 RyuJIT AVX2


```
| Method           | Mean      | Error     | StdDev    |
|----------------- |----------:|----------:|----------:|
| VirtualMetod     |  10.07 ms |  0.106 ms |  0.094 ms |
| IntefaceMetod    |  17.38 ms |  0.309 ms |  0.289 ms |
| ReflectionMethod | 655.21 ms | 13.044 ms | 16.496 ms |
